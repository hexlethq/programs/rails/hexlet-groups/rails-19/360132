# frozen_string_literal: true

# class ExecutorTimer
class ExecutionTimer
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    before = Time.now.to_f
    status, headers, body = @appl.call(env)
    after = Time.now.to_f
    rtime = ((after - before) * 1000).round(5)
    message = " ...Response Time: #{rtime} ms. "
    [status, headers, body << message]
  end
end
