# frozen_string_literal: true

require 'digest'

# class Signature
class Signature
  def initialize(appl)
    @appl = appl
  end

  def call(env)
    status, headers, body = @appl.call(env)
    bodynew = Digest::SHA256.hexdigest(body[0])
    [status, headers, body << bodynew]
  end
end
