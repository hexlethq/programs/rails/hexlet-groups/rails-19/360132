# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_push
    @stack.push!('ruby')
    @stack.push!('php')
    @stack.push!('java')
    expected = %w[ruby php java]
    assert_equal expected, @stack.to_a
    assert(@stack.size == 3)
    refute_empty(@stack)
  end

  def test_pop
    @stack.push!('ruby')
    @stack.push!('php')
    @stack.push!('java')
    @stack.pop!
    expected = %w[ruby php]
    assert_equal expected, @stack.to_a
    assert(@stack.size == 2)
  end

  def test_clear
    @stack.push!('ruby')
    @stack.push!('php')
    @stack.push!('java')
    @stack.pop!
    @stack.clear!
    expected = []
    assert_equal expected, @stack.to_a
    assert_empty(@stack)
  end

  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
