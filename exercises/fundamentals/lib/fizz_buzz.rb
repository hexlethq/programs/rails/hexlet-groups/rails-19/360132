# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  str = ''
  return str if stop < start

  start.upto stop do |i|
    str = "#{str} " if i != start
    st = ''
    if (i % 3 != 0) && (i % 5 != 0)
      st = i.to_s
    else
      st = (i % 3).zero? ? 'Fizz' : ''
      st += (i % 5).zero? ? 'Buzz' : ''
    end
    str = "#{str}#{st}"
  end
  str
end
# END
