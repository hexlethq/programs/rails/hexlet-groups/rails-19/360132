# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  if num.negative?
    nil
  elsif num <= 1
    0
  elsif num == 2
    1
  else
    n1 = 0
    n2 = 1
    counter = 3
    while counter <= num
      element = n1 + n2
      counter += 1
      n1 = n2
      n2 = element
    end
    element
  end
end

# END
