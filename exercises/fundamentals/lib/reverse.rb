# frozen_string_literal: true

# BEGIN
def reverse(str)
  strreverse = ''
  str.each_char do |c|
    strreverse = "#{c}#{strreverse}"
  end
  strreverse
end
# END
