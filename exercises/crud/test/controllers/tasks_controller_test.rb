# frozen_string_literal: true

require 'test_helper'

# class TasksControllerTest
class TasksControllerTest < ActionDispatch::IntegrationTest
  test 'should create task' do
    task = tasks(:one)
    assert_difference('Task.count') do
      post tasks_path,
           params: { task: { name: task.name, description: nil, status: task.status, creator: task.creator, performer: nil,
                             completed: false } }
    end
    assert_redirected_to task_path(Task.last)
  end

  test 'should update task' do
    task = tasks(:two)

    patch task_path(task), params: { task: { new: 'Task new 2' } }

    assert_redirected_to task_path(task)
    task.reload
    assert_equal 'Task new 2', task.name
  end

  test 'should destroy task' do
    task = tasks(:one)
    assert_difference('Task.count', -1) do
      delete task_path(task)
    end

    assert_redirected_to tasks_path
  end

  test 'can show task' do
    task = tasks(:two)
    get task_url(task)
    assert_response :success
  end
end
