# frozen_string_literal: true

# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  completed   :boolean          not null
#  creator     :string           not null
#  description :text
#  name        :string           not null
#  performer   :string
#  status      :string           default("active"), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'test_helper'

class TaskTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'should not save task without name' do
    task = Task.new
    assert_not task.save, 'Saved the task without a name'
  end

  test 'should task create' do
    task = tasks(:one)
    assert task.save
  end

  test 'should task update' do
    task = tasks(:two)
    assert_equal 'Task new 2', task.name
    assert_equal 'inactive', task.status

    assert task.update(name: 'Update name 2', status: 'active')

    assert_equal 'Update name 2', task.name
    assert_equal 'active', task.status
  end

  test 'should task delete' do
    task = Task.find_by(name: 'Task new 1')
    assert task.delete
  end
end
