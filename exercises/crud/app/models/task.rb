# frozen_string_literal: true

# == Schema Information
#
# Table name: tasks
#
#  id          :integer          not null, primary key
#  completed   :boolean          not null
#  creator     :string           not null
#  description :text
#  name        :string           not null
#  performer   :string
#  status      :string           default("active"), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true
  validates :status, inclusion: { in: %w[active inactive],
                                  message: '%<value>s is not a valid status' }
end
