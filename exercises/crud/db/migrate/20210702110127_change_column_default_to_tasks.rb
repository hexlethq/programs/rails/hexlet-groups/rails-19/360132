# frozen_string_literal: true

# class ChangeColumnDefaultToTasks
class ChangeColumnDefaultToTasks < ActiveRecord::Migration[6.1]
  def change
    change_column_default :tasks, :status, from: 'new', to: 'active'
  end
end
