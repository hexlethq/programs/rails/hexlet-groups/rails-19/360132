# frozen_string_literal: true

Rails.application.routes.draw do
  root 'homes#index'

  resources :posts do
    resources :comments, only: %i[new create edit update destroy], module: 'post'
  end
end