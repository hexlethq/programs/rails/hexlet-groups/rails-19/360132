class Post::CommentsController < ApplicationController
  before_action :set_post, only: %i[create edit]
  before_action :set_post_comment, only: %i[edit update destroy]

  # GET /post/comments/1/edit
  def edit; end

  def create
    @post_comment = @post.comments.build(post_comment_params)
    if @post_comment.save
      redirect_to post_path(@post), notice: 'Comment was successfully created.'       
    else
      render 'posts/show', status: :unprocessable_entity 
    end
  end

  def update
    if @post_comment.update(post_comment_params)
      redirect_to post_path(@post_comment.post_id), notice: 'Comment was successfully updated.' 
    else
      render :edit, status: :unprocessable_entity 
    end
  end

  def destroy
    @post_comment.destroy
    redirect_to post_path(@post_comment.post_id), notice: 'Comment was successfully destroyed.' 
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post 
    @post = Post.find(params[:post_id])
  end

  def set_post_comment
    @post_comment = Post::Comment.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def post_comment_params
    params.require(:post_comment).permit(:body, :post_id)
  end
end
