# Webpacker

## Ссылки

* [Rails Guides — что такое Webpacker](https://edgeguides.rubyonrails.org/webpacker.html)
* [Установка Font Awesome с помощью пакетного менеджера](https://fontawesome.com/v5.15/how-to-use/on-the-web/setup/using-package-managers)
* [Использование Font Awesome на странице](https://fontawesome.com/v5.15/how-to-use/on-the-web/referencing-icons/basic-use)
* [How to Install FontAwesome with Yarn and Webpacker in Rails 6?](https://dev.to/yarotheslav/how-to-install-fontawesome-with-yarn-and-webpacker-in-rails-6-2k62)

## Задачи

Выполните `make setup`,

### app/javascript/packs/application.js

Импортируйте библиотеку Fort Awesome

### app/views/layouts/application.html.slim

Подключите стили и javascript скрипты в шапку шаблона.

### app/views/layouts/shared/_nav.html.slim

Добавьте навигацию с иконками:

* `home` - домашняя страница
* `users` - список пользователей
* `tasks` - список задач
* ...

## Add User entity

### Добавлено

* app/controllers/users_controller.rb
* app/models/user.rb
* app/views/users/_form.html.slim
* app/views/users/edit.html.slim
* app/views/users/index.html.slim
* app/views/users/new.html.slim
* app/views/users/show.html.slim
* db/migrate/20210708112926_create_users.rb
* test/controllers/users_controller_test.rb

### Изменено

* app/views/layouts/shared/_nav.html.slim
* config/routes.rb

## Add user seeds

### Изменено

* db/seeds.rb
* Gemfile (bundle add faker)
