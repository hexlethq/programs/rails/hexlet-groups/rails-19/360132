# frozen_string_literal: true

# BEGIN
def make_censored(text, stop_words)
  arraytext = text.split(/ /)
  arraytext.map! do |word|
    word1 = word
    stop_words.each { |wrd| word1 = '$#%!' if word1 == wrd }
    word1
  end
  arraytext.join(' ')
end
# END
