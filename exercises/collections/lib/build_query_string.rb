# frozen_string_literal: true

# BEGIN
def build_query_string(param)
  array = param.keys
  array.sort!
  array.map! { |key| "#{key}=#{param[key]}" }
  array.join('&')
end

# END
