# frozen_string_literal: true

# BEGIN
def compare_versions(ver1, ver2)
  array1 = ver1.split('.')
  array2 = ver2.split('.')
  index = array1[0].to_i == array2[0].to_i ? 1 : 0
  array1[index].to_i <=> array2[index].to_i
end
# END
