# frozen_string_literal: true

require 'application_system_test_case'

class VacanciesTest < ApplicationSystemTestCase

  test "published a vacancy" do
    visit vacancies_path
    click_on "Publish", match: :first

    assert_text "Vacancy was published."
  end

  test "archived a vacancy" do
    visit vacancies_path
    click_on "Archive", match: :first

    assert_text "Vacancy was archived."
  end

end