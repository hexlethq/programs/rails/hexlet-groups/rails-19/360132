# frozen_string_literal: true

require 'test_helper'

class Vacanciestest < ActionDispatch::IntegrationTest
  test "on_moderate_to_publish" do
    vacancy = vacancies(:on_moderate)
    vacancy.publish!
    assert vacancy.reload.published?
  end

  test "on_moderate_to_archive" do
    vacancy = vacancies(:on_moderate)
    vacancy.archive!
    assert vacancy.reload.archived?
  end

  test "published_to_archive" do
    vacancy = vacancies(:published)
    vacancy.archive!
    assert vacancy.reload.archived?
  end

  test "archived_not_to_published" do
    vacancy = vacancies(:archived)
    vacancy.publish!
    assert vacancy.reload.archived?
  end

end
