# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  us = users.select { |el| el[:gender] == 'male' }
  years = us.map do |ind|
    ind[:birthday][0..3]
  end
  years.each_with_object(Hash.new(0)) { |el, hash| hash[el] += 1 }
end

# END
