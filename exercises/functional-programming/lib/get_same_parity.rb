# frozen_string_literal: true

# BEGIN
def get_same_parity(array)
  array.delete_if { |x| x.even? != array[0].even? }
end
# END
