# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, array)
  array.select { |el| el.upcase.chars.sort.join == word.upcase.chars.sort.join }
end
# END
