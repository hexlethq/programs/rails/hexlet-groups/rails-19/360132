# frozen_string_literal: true

require 'test_helper'

class BulletinsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get bulletins_path

    assert_response :success
    assert_equal 'index', @controller.action_name
    assert_match 'Bulletins', @response.body
  end

  test 'should get show' do
    get bulletin_path(bulletins(:bulletin_one))
    assert_response :success
    assert_equal 'show', @controller.action_name
  end
end
