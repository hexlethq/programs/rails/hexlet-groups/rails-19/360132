# frozen_string_literal: true

require 'test_helper'

class BulletinsFlowTest < ActionDispatch::IntegrationTest
  test 'bulletins index action' do
    get bulletins_path

    assert_equal '/bulletins', path
    assert_select 'h1', 'Bulletins'
  end

  test 'bulletin show action' do
    get bulletin_path(bulletins(:bulletin_one))

    assert_select 'h1', 'Title 1'
    assert_select 'p', 'Body 1'
    assert_select 'p', 'false'
    assert_select 'p', 'Created: 2021-06-30'

    get bulletin_path(bulletins(:bulletin_two))
    assert_select 'p', 'true'
    assert_select 'p', 'Created: 2021-06-29'
  end
end
