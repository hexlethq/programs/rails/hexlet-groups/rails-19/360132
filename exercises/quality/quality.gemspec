# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.required_ruby_version = '>= 3.0'
  spec.version = '0.1.0'
  spec.name    = 'quality'
  spec.summary = 'Rails Basics'
  spec.authors = ['Hexlet']
  spec.files   = `git ls-files`
end
