# frozen_string_literal: true

# BEGIN
module Model
  def self.included(base)
    base.extend(ClassMethods)
  end

  def initialize(attributes = nil)
    @attr = {}
    self.class.scheme.each_key do |name|
      @attr[name] = if attributes.nil?
                      self.class.attr_convert(name, nil, true)
                    else
                      self.class.attr_convert(name, attributes[name], !attributes.key?(name))
                    end
    end
  end

  def attributes
    @attr
  end

  # class
  module ClassMethods
    def scheme
      @scheme ||= {}
    end

    def attribute(name, options = {})
      scheme[name] = options

      define_method name do
        @attr[name]
      end

      define_method "#{name}=" do |value|
        @attr[name] = self.class.attr_convert(name, value, false)
      end
    end

    def attr_convert(name, value, isdef)
      return scheme[name][:default] if isdef

      return nil if value.nil?

      type = scheme[name][:type].to_s
      case type
      when 'integer' then value.to_i
      when 'string' then value.to_s
      when 'datetime' then DateTime.parse(value.to_s)
      when 'boolean' then boolean(value)
      else
        value
      end
    end

    def boolean(value)
      case value
      when true, 'true', 1, '1', 'yes' then true
      when false, 'false', 0, '0', 'no' then false
      end
    end
  end
end
# END
