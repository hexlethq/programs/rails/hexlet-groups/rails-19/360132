# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_user
    attributes = { birthday: '13/01/2013', active: true }
    result_attributes = { name: 'Andrey', birthday: DateTime.parse('13/01/2013'), active: true }
    user = User.new(attributes)

    assert_equal result_attributes, user.attributes

    attributes = { name: nil }
    result_attributes = { name: nil, birthday: nil, active: false }
    user = User.new(attributes)

    assert_equal result_attributes, user.attributes
  end

  def test_attribute_accessors # rubocop:disable Metrics/AbcSize
    attributes = { name: 'Svetlenkaja', birthday: '13/01/2013', active: false }
    user = User.new attributes

    assert_equal attributes[:name], user.name
    assert_equal DateTime.parse(attributes[:birthday]), user.birthday
    assert_equal attributes[:active], user.active
    new_attributes = { name: nil, birthday: '31/12/2000', active: true }
    user.name = new_attributes[:name]
    user.birthday = new_attributes[:birthday]
    user.active = new_attributes[:active]

    assert_nil user.name
    assert_equal DateTime.parse(new_attributes[:birthday]), user.birthday
    assert_equal new_attributes[:active], user.active
  end

  def test_users_not_equals
    user1 = User.new name: 'Svetlenkaja'
    user2 = User.new

    assert_equal 'Svetlenkaja', user1.name
    assert_equal 'Andrey', user2.name
    refute_equal user1.name, user2.name
  end

  def test_attribute_convert
    user = User.new name: 123_456, birthday: '31/12/2000', active: 'no'
    result_attributes = { name: '123456', birthday: DateTime.parse('31/12/2000'), active: false }

    assert_equal result_attributes, user.attributes
    assert_instance_of DateTime, user.birthday
  end
  # END
end
