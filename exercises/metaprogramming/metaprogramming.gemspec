# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.required_ruby_version = '>= 2.5'
  spec.version = '0.1.0'
  spec.name    = 'metaprogramming'
  spec.summary = 'First Module'
  spec.authors = ['Hexlet']
  spec.files   = `git ls-files`
end
