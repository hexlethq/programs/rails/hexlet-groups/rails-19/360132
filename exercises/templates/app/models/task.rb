# frozen_string_literal: true

# class Task
class Task < ApplicationRecord
  validates :name, :status, :creator, presence: true
  validates :status, inclusion: { in: %w[active inactive],
                                  message: '%<value>s is not a valid status' }
  validates :completed, inclusion: [true, false]
end
