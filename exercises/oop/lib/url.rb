# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

# class Url
class Url
  attr_accessor :url

  extend Forwardable
  def_delegators :@url, :scheme, :host
  include Comparable
  def initialize(str)
    @url = URI(str)
    @hash = URI.decode_www_form(url.query || '').to_h.transform_keys!(&:to_sym)
  end

  def <=>(other)
    url <=> other.url
  end

  def query_params
    @hash
  end

  def query_param(param1, param2 = nil)
    @hash.fetch(param1, param2)
  end
end
# END
