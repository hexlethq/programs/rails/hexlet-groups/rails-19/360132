# frozen_string_literal: true

require 'open-uri'
require 'nokogiri'
require 'net/http'
require 'net/https'
require 'json'

# rubocop:disable Metrics/AbcSize
class Hacker
  class << self
    def hack(email, password)
      hostname = ('https://rails-l4-collective-blog.herokuapp.com')

      uri = URI.parse("#{hostname}/users/sign_up")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = uri.scheme == 'https'
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request_get = Net::HTTP::Get.new(uri.request_uri)
      response_get = http.request(request_get)
      cookie = response_get.response['set-cookie'].split('; ')[0]
      
      params = {
        'user[email]': email,
        'user[password]': password,
        'user[password_confirmation]': password,
        'authenticity_token': token(response_get.body)
      }

      request_post = Net::HTTP::Post.new "#{hostname}/users"
      request_post.body = URI.encode_www_form(params)
      request_post['Cookie'] = cookie
      response_post = http.request(request_post)

      response_post.code == '302' ? 'Registered.' : 'Not Registered.'
    end

    def token(response_body)
      html = Nokogiri::HTML(response_body)
      token_tag = html.at('input[@name="authenticity_token"]')
      token_tag['value']
    end
  end
end
# rubocop:enable Metrics/AbcSize
