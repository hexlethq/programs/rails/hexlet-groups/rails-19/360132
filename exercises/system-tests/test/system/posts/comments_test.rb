# frozen_string_literal: true

require 'application_system_test_case'

class CommentsTest < ApplicationSystemTestCase
  setup do
    @post = posts(:one)
    @comment = @post.comments
  end

  test "creating a Comment" do
    visit posts_url
    click_on "Show", match: :first

    fill_in "post_comment_body", with: @comment.first.body
    click_on "Create Comment"
    
    assert_text "Comment was successfully created."
    click_on "Back"
  end

  test "updating a Comment" do
    visit edit_post_comment_path(@post, @comment.first)

    fill_in "Body", with: @comment.first.body
    click_on "Update Comment"

    assert_text "Comment was successfully updated."
    click_on "Back"
  end

  test "destroying a Comment" do
    visit posts_url
    click_on "Show", match: :first

    page_before_destroy = current_path

    page.accept_confirm do
      click_on "Delete", match: :first
    end
   
    assert_current_path page_before_destroy
  end
end