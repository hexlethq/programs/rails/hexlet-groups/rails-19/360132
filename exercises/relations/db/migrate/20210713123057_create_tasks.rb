class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :status, indaex: true, foreign_key: true
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
