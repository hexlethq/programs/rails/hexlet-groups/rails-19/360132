require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:two)
    @status = statuses(:two)
    @user = users(:two)
  end

  test 'should get index' do
    get tasks_path

    assert_response :success
    assert_equal 'index', @controller.action_name
    assert_match 'Tasks', @response.body
  end

  test 'should create task' do
    assert_difference('Task.count') do
      post tasks_path,
           params: { task: { name: @task.name, description: @task.description, user_id: @user.id,
                             status_id: @status.id } }
    end
    assert_redirected_to task_path(Task.last)
  end

  test 'should update task' do
    patch task_path(@task), params: { task: { new: @task.name } }

    assert_redirected_to task_path(@task)
    @task.reload
    assert_equal 'MyTask2', @task.name
  end

  test 'should destroy task' do
    assert_difference('Task.count', -1) do
      delete task_path(@task)
    end

    assert_redirected_to tasks_path
  end

  test 'can show task' do
    get task_url(@task)
    assert_response :success
  end
end
